import './App.css';
import Header from './page/Header/index'
import AppBar from "./page/AppBar"
import Slide from "./component/Slide"
import TopCategori from "./page/TopCategori"
import TopTrending from "./page/TopTrending"
import Information from "./page/Information";
import FolowUs from "./page/FolowUs"
import Footer from "./page/Footer"

function App() {
  return (
    <div className="App">
      <Header />
      <AppBar />
      <Slide />
      <TopCategori />
      <TopTrending />
      <Information />
      <FolowUs/>
      <Footer/>
    </div>
  );
}

export default App;

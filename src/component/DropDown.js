import React from "react";
import styled from "styled-components";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";

const DropDownContent = styled.div`
  display: none;
  position: absolute;
  top: 18px;
  left: 0;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
  font-size: 13px;
  background-color: #FFFF;
`;

const DropDownContentItem = styled.a`
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  &:hover {
    color: #ffa500;
  }
`;
const Title = styled.span`
  font-size: 13px;
  line-height: 18px
  color: #000;
  font-weight: ${(props) => props.fontWeight};
`;

const DropDownCom = styled.div`
  display: flex;
  margin: ${ props => props.marginLeft ? 'auto auto auto 25px' : 'auto 25px auto auto'};
  align-items: center;
  position: relative;
  &:hover ${DropDownContent} {
    display: block;
  }
`;
export default function DropDown(props) {
  return (
    <DropDownCom marginLeft={props?.marginLeft}>
      <Title fontWeight={props.fontWeight? props.fontWeight : 500}>
        {props.title}
      </Title>
      <KeyboardArrowDownIcon fontSize="small" />
      <DropDownContent>
        {props.subList ? (
          props.subList.map((value) => (
            <DropDownContentItem key={value}>{value}</DropDownContentItem>
          ))
        ) : (
          <div></div>
        )}
      </DropDownContent>
    </DropDownCom>
  );
}

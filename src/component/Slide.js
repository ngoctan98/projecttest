import React, { useState } from "react";

import { makeStyles } from "@mui/styles";
import Img from "../assets/black-white-and-gold-bedroom.jpeg"
import Img2 from "../assets/a-slattum-upholstered-bed-and-various-white-nordli-chests-of-8d484dc7644efac6f63d4e9da13cfbe3.avif";
import Img3 from "../assets/depositphotos_273925290-stock-photo-blue-and-black-bedroom-with.jpeg"
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

const useStyles = makeStyles((them) => ({
  slideshowContainer: {
    maxWidth: 1860,
    maxHeight: 798,
    position: "relative",
    margin: "auto",
    boderRadius: 20,
    overflow: "hidden",
  },
  mySlides: {
    borderRadius: 20,
    overflow: "hidden",
    marginTop: 30,
  },
  prev: {
    cursor: "pointer",
    position: "absolute",
    top: "50%",
    width: 60,
    height: 60,
    lineHeight: 60,
    marginTop: -30,
    marginLeft: 30,
    color: "#111111",
    fontSize: 18,
    transition: "0.6s ease",
    borderRadius: "50%",
    userSelect: "none",
    backgroundColor: "#EBEBEB",
    alignItems: "center",
    opacity: 0.4,
    "&:hover": {
      backgroundColor: "#111111",
      color: "white",
    },
  },
  iconPrev: {
    position: "absolute",
    top: "50%",
    width: 60,
    height: 60,
    marginLeft: -5,
    marginTop: -10,
  },
  iconNext: {
    position: "absolute",
    top: "50%",
    width: 60,
    height: 60,
    marginLeft: -10,
    marginTop: -10,
  },
  next: {
    cursor: "pointer",
    position: "absolute",
    top: "50%",
    width: 60,
    height: 60,
    lineHeight: 60,
    marginTop: -30,
    marginRight: 30,
    color: "#111111",
    fontSize: 18,
    transition: "0.6s ease",
    borderRadius: "50%",
    userSelect: "none",
    backgroundColor: "#EBEBEB",
    opacity: 0.4,
    "&:hover": {
      backgroundColor: "#111111",
      color: "white",
    },
    right: 0,
  },
  text: {
    color: "#f2f2f2",
    fontSize: 15,
    padding: "8px 12px",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    textAlign: "center",
    display: "flex",
  },
  numbertext: {
    color: "#f2f2f2",
    fontSize: 12,
    padding: "8px 12px",
    position: "absolute",
    top: 0,
  },
  wraperDot: {
    textAlign: "center",
    position: "absolute",
    bottom: 30,
    width: "100%",
    "@media (max-width: 740px)": {
      bottom: 10
    },
  },
  dot: {
    cursor: "pointer",
    height: 8,
    width: 8,
    margin: "0 8px",
    backgroundColor: "#bbb",
    borderRadius: "50%",
    display: "inline-block",
    transition: "background-color 0.6s ease",
  },
  dotActive: {
    cursor: "pointer",
    height: 8,
    width: 8,
    margin: "0 2px",
    backgroundColor: "#000",
    borderRadius: "50%",
    display: "inline-block",
    transition: "background-color 0.6s ease",
  },
  img: {
    width: "100%",
    maxWidth: 1860,
    maxHeight: 798,
  },
  firstText: {
    fontSize: 13,
    lineHeight: "18px",
    fontWeight: 600,
    color: "#FFFFF",
    "@media (max-width: 1024px)": {
      fontSize: 10,
      lineHeight: "16px",
      marginTop: 5,
    },
    "@media (max-width: 740px)": {
      fontSize: 10,
      lineHeight: "14px",
      marginTop: 0,
    },
  },
  textPlan: {
    fontSize: 90,
    lineHeight: "122px",
    fontWeight: 600,
    color: "#FFFFF",
    margin: 0,
    "@media (max-width: 1024px)": {
      fontSize: 60,
      lineHeight: "86px",
    },
    "@media (max-width: 740px)": {
      fontSize: 30,
      lineHeight: "33px",
    },
  },
  textDiscount: {
    fontSize: 16,
    lineHeight: "18px",
    fontWeight: 500,
    color: "#FFFFF",
    "@media (max-width: 1024px)": {
      fontSize: 14,
      lineHeight: "16px",
    },
    "@media (max-width: 740px)": {
      fontSize: 12,
      lineHeight: "14px",
    },
  },
  button: {
    width: 180,
    height: 55,
    marginTop: 50,
    borderRadius: 30,
    fontWeight: 600,
    border: "none",
    bạckgroundColor: "#FFFFF",
    "@media (max-width: 1024px)": {
      fontSize: 10,
      lineHeight: "16px",
      marginTop: 5,
      width: 150,
      height: 40,
    },
    "@media (max-width: 740px)": {
      fontSize: 10,
      lineHeight: "16px",
      width: 100,
      height: 25,
      marginTop: 0,
    },
  },
}));

 const Slide = () => {
    const classes = useStyles();
    const [slide, setSlide]=useState(1)
    const handleClickDot = (value) =>{
        setSlide(value)
    }
    const handleClickNext = () => {
        slide === 3 ? setSlide(1) : setSlide(slide+1)
        
    }
    const handleClicPrev = () => {
      slide === 1 ? setSlide(3) : setSlide(slide - 1);
    };

    return (
      <div className={classes.slideshowContainer}>
        {slide === 1 && (
          <div className={classes.mySlides}>
            <img src={Img} className={classes.img} alt="kjsdffhf" />
            <div className={classes.text}>
              <div style={{ margin: "auto" }}>
                <p className={classes.firstText}>BEAUTIFUL & ELEGANT</p>
                <p className={classes.textPlan}>Premium Bedroom</p>
                <p className={classes.textDiscount}>
                  Discount 50% On Products & Free Shipping.
                </p>
                <button className={classes.button}>SHOP NOW</button>
              </div>
            </div>
          </div>
        )}

        {slide === 2 && (
          <div className={classes.mySlides}>
            <img src={Img2} className={classes.img} alt="kjsdffhf" />
            <div className={classes.text}>
              <div style={{ margin: "auto" }}>
                <p className={classes.firstText}>BEAUTIFUL & ELEGANT</p>
                <p className={classes.textPlan}>Premium Bedroom</p>
                <p className={classes.textDiscount}>
                  Discount 50% On Products & Free Shipping.
                </p>
                <button className={classes.button}>SHOP NOW</button>
              </div>
            </div>
          </div>
        )}
        {slide === 3 && (
          <div className={classes.mySlides}>
            <img src={Img3} className={classes.img} alt="kjsdffhf" />
            <div className={classes.text}>
              <div style={{ margin: "auto" }}>
                <p className={classes.firstText}>BEAUTIFUL & ELEGANT</p>
                <p className={classes.textPlan}>Premium Bedroom</p>
                <p className={classes.textDiscount}>
                  Discount 50% On Products & Free Shipping.
                </p>
                <button className={classes.button}>SHOP NOW</button>
              </div>
            </div>
          </div>
        )}

        <p className={classes.prev}>
          <ArrowBackIosIcon
            onClick={handleClicPrev}
            className={classes.iconPrev}
          />
        </p>
        <p onClick={() => handleClickNext()} className={classes.next}>
          <ArrowForwardIosIcon className={classes.iconNext} />
        </p>
        <div className={classes.wraperDot}>
          <span
            onClick={() => {
              handleClickDot(1);
            }}
            className={classes[slide === 1 ? "dotActive" : "dot"]}
          ></span>
          <span
            onClick={() => {
              handleClickDot(2);
            }}
            className={classes[slide === 2 ? "dotActive" : "dot"]}
          ></span>
          <span
            onClick={() => {
              handleClickDot(3);
            }}
            className={classes[slide === 3 ? "dotActive" : "dot"]}
          ></span>
        </div>
      </div>
    );
    }

export default  Slide;
import React from "react";

const HeaderContent = ({header, content}) => {
  return (
  <div style={{width: '100%', alignItems: 'center'}}>
    <p style={{fontSize: 36, lineHeight: '49px', margin: 0}}>{header}</p>
    <p style={{fontSize: 14, lineHeight: '19px', margin: '5px 0 0 0'}}>{content}</p>
  </div>
  );
};
export default HeaderContent;

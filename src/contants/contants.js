import React from "react"
import Inventory2OutlinedIcon from "@mui/icons-material/Inventory2Outlined";
import VerifiedOutlinedIcon from "@mui/icons-material/VerifiedOutlined";
import ContactSupportOutlinedIcon from "@mui/icons-material/ContactSupportOutlined";
import img1 from "../assets/5ddb32b0b81b0407013a.webp";
import img2 from "../assets/a-slattum-upholstered-bed-and-various-white-nordli-chests-of-8d484dc7644efac6f63d4e9da13cfbe3.avif";
import img3 from "../assets/black-white-and-gold-bedroom.jpeg";
import img4 from "../assets/depositphotos_273925290-stock-photo-blue-and-black-bedroom-with.jpeg";
import img5 from "../assets/images.jpeg";
import img6 from "../assets/img-20190730-wa3165-500x500.webp";

export const listTopCategori = [
  {
    img: img1,
    content: "Living Room",
  },
  {
    img: img2,
    content: "Special Bedroom",
  },
  {
    img: img3,
    content: "Dining & Kitchen",
  },
  {
    img: img4,
    content: "Home Accessories",
  },
  {
    img: img5,
    content: "Outdoor",
  },
  {
    img: img6,
    content: "Lighting & Decor",
  },
];

export const listFolowUs = [
  {
    img: img1,
    content: "Living Room",
  },
  {
    img: img2,
    content: "Special Bedroom",
  },
  {
    img: img3,
    content: "Dining & Kitchen",
  },
  {
    img: img4,
    content: "Home Accessories",
  },
  {
    img: img5,
    content: "Outdoor",
  },
];

export const listTopTrending = [
  {
    img: img1,
    content: "Form Chair Brass Base",
    discount: "50%",
    preOrder: "Pre-order",
    costOld: "$799",
    cost: "$399",
  },
  {
    img: img2,
    content: "Vase Echasse Large",
    cost: "$399",
  },
  {
    img: img3,
    content: "Bottle Grinder Of Pepper",
    cost: "$399",
  },
  {
    img: img4,
    content: "Form Bar Stool Oak Base",
    cost: "$399",
  },
  {
    img: img5,
    content: "Table Lamp 15x29cm",
    cost: "$399",
  },
  {
    img: img6,
    content: "Lighting & Decor",
    discount: "50%",
    preOrder: "Pre-order",
    costOld: "$799",
    cost: "$399",
  },
  {
    img: img2,
    content: "Carrie Table Lamp",
    cost: "$399",
  },
  {
    img: img3,
    content: "Dining & Kitchen",
    cost: "$399",
  },
];

export const listInformation = [
  {
    icon: <Inventory2OutlinedIcon style={{ fontSize: 35 }} />,
    title: "Free Shipping",
    content: `Get complimentary ground shipping on every order.Don't love it? Send it back, on us.`,
  },
  {
    icon: <VerifiedOutlinedIcon style={{ fontSize: 35 }} />,
    title: "Free Returns",
    content: `Free returns within 10 days, please make sure the items are in undamaged condition.`,
  },
  {
    icon: <ContactSupportOutlinedIcon style={{ fontSize: 35 }} />,
    title: "Support Online",
    content: `We support customers 24/7, send questions we will solve for you immediately.`,
  },
];
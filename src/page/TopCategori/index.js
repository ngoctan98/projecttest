import React, {useState} from 'react';
import HeaderContent from "../../component/HeaderContent"
import TopCategoriItem from './TopCategoriItem';
import {listTopCategori} from "../../contants/contants"
import {useStyles} from "./styles"
import LazyLoad from "react-lazyload";


const TopCategori = () => {
      const [width, setWidth] = useState(1910);
      window.addEventListener(
        "resize",
        function (event) {
          setWidth(event.currentTarget.innerWidth);
        },
        true
      );
    const classes=useStyles()
    return (
      <div className={classes.root}>
        <HeaderContent
          header={"Top Categories"}
          content={
            "Our products are designed for everyone, environmentally friendly."
          }
        />
        <div className={classes.wraperListTopCategori}>
          {listTopCategori.map((item, index) => {
            return (
              <LazyLoad>
                <div key={item.img}>
                  {width > 1024 && (
                    <TopCategoriItem img={item.img} content={item.content} />
                  )}
                  {width < 1024 && width > 740 && index < 4 && (
                    <TopCategoriItem img={item.img} content={item.content} />
                  )}
                  {width < 741 && index < 2 && (
                    <TopCategoriItem img={item.img} content={item.content} />
                  )}
                </div>
              </LazyLoad>
            );
          })}
        </div>
        <button className={classes.buttonLoadMore}>LOAD MORE</button>
      </div>
    );
}
export default TopCategori
import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((them) => ({
  root: {
    margin: "100px auto 0 auto",
    display: "flex",
    flexDirection: "column",
    maxWidth: 1410,
    "@media (max-width:1024px)": {
      margin: "50px auto 0 auto",
    },
  },
  wraperListTopCategori: {
    display: "flex",
    padding: 15,
    justifyContent: "center",
    marginTop: 40,
    width: "100%",
    "@media (max-width:1024px)": {
      overflow: "hidden",
    },
    "@media (max-width:740px)": {
      justifyContent: "center",
    },
  },
  wraperItem: {
    width: 160,
    padding: "0 15px",
    alignContent: "center",
    "@media (max-width:1024px)": {
      padding: "0 15px",
    },
  },
  avatar: {
    width: 160,
    height: 160,
  },
  content: {
    fontSize: 16,
    lineHeight: "22px",
    textAlign: "center",
    marginTop: 15,
  },
  buttonLoadMore: {
    fontSize: 12,
    fontWeight: 600,
    backgroundColor: "#FFFF",
    border: "1px solid #DEDEDE",
    borderRadius: 300,
    height: 50,
    width: 200,
    margin: "40px auto 0",
    "@media (min-width: 1025px)": {
      display: "none",
    },
    "@media (max-width: 1024px)": {
      height: 40,
      width: 150,
      fontSize: 10,
      margin: "20px auto 0",
    },
  },
}));
import React from "react";
import Avatar from "@mui/material/Avatar";
import {useStyles} from "./styles"

const TopCategoriItem = ({content, img}) => {
    const classes=useStyles()
  return (
    <div className={classes.wraperItem}>
      <Avatar sx={{width: 160, height: 160}} src={img} />
      <p className={classes.content}>{content}</p>
    </div>
  );
};
export default TopCategoriItem;

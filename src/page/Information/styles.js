import { makeStyles } from "@mui/styles";

 export const useStyles = makeStyles((them) => ({
   slideshowContainer: {
     maxWidth: 1860,
     maxHeight: 798,
     position: "relative",
     margin: "auto",
     boderRadius: 20,
     overflow: "hidden",
   },
   mySlides: {
     borderRadius: 20,
     overflow: "hidden",
     marginTop: 30,
   },
   text: {
     color: "#f2f2f2",
     fontSize: 15,
     padding: "8px 12px",
     position: "absolute",
     top: 0,
     left: 0,
     width: "100%",
     height: "100%",
     textAlign: "center",
     display: "flex",
     "@media (max-width: 740px)": {
       position: "initial",
     },
   },
   img: {
     width: "100%",
     maxWidth: 1860,
     maxHeight: 798,
   },
   firstText: {
     fontSize: 12,
     lineHeight: "16px",
     fontWeight: 500,
     color: "#999999",
     margin: "50px 0 0 0",
     "@media (max-width: 1024px)": {
       margin: "30px 0 0 0",
     },
   },
   textPlan: {
     fontSize: 40,
     lineHeight: "54px",
     fontWeight: 600,
     color: "#111111",
     margin: "10px 0 0 0",
     "@media (max-width: 1024px)": {
       fontSize: 32,
       lineHeight: "44px",
       margin: "5px 0 0 0",
     },
   },
   textDiscount: {
     fontSize: 14,
     lineHeight: "24px",
     fontWeight: 500,
     color: "#555555",
     width: 460,
     margin: "20px auto 0",
     "@media (max-width: 1024px)": {
       width: 400,
       margin: "10px auto 0",
       fontSize: 12,
       lineHeight: "20px",
     },
   },
   button: {
     width: 180,
     height: 55,
     marginTop: 50,
     marginBottom: 50,
     borderRadius: 30,
     fontWeight: 600,
     border: "none",
     backgroundColor: "#111111",
     color: "#FFF",
     "@media (max-width: 1024px)": {
       width: 150,
       height: 40,
       marginTop: 30,
       marginBottom: 30,
       fontSize: 12,
       lineHeight: "20px",
     },
   },
   wraperContent: {
     margin: "auto 225px auto auto ",
     width: 560,
     borderRadius: 20,
     backgroundColor: "#FFFF",
     "@media (max-width: 1024px)": {
       maxWidth: 460,
       margin: "auto 105px auto auto ",
     },
     "@media (max-width: 740px)": {
       maxWidth: 300,
       margin: "auto 105px auto auto ",
     },
   },
   wraperInformation: {
     display: "flex",
     maxWidth: 1220,
     margin: "100px auto 0",
     "@media (max-width: 1024px)": {
       margin: "50px auto 0",
     },
   },
   listInformation: {
     display: "flex",
     justifyContent: "space-between",
     width: "100%",
     "@media (max-width: 740px)": {
       flexDirection: "column",
       alignItems: "center",
     },
   },
 }));

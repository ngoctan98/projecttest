import React from "react"
const InformationItem = ({title, content, icon}) => {
    return (
      <div style={{ maxWidth: 340 }}>
        {icon}
        <p style={{ fontSize: 16, fontWeight: 600, lineHeight: "22px", margin: "10px 0 0 0" }}>
          {title}
        </p>
        <p style={{ fontSize: 14, lineHeight: "19px", margin: "10px 0 0 0" }}>
          {content}
        </p>
      </div>
    );
}
export default InformationItem;
import React from "react";
import {useStyles} from "./styles.js"
import Img from "../../assets/black-white-and-gold-bedroom.jpeg";
import InformationItem from "./InformationItem"
import {listInformation} from "../../contants/contants"
import LazyLoad from "react-lazyload";


const Information = () => {
  const classes = useStyles();

  return (
    <div>
      <div className={classes.slideshowContainer}>
        <div className={classes.mySlides}>
          <img src={Img} className={classes.img} alt="kjsdffhf" />
          <div className={classes.text}>
            <div className={classes.wraperContent}>
              <p className={classes.firstText}>LIMITED EDITION</p>
              <p className={classes.textPlan}>Unique Style</p>
              <p className={classes.textDiscount}>
                The watch is carefully designed with quality materials, such as
                the domed sapphire crystal, and the meticulous level of detail -
                from the dial to the delicate gold hour markers.
              </p>
              <button className={classes.button}>DISCOVER NOW</button>
            </div>
          </div>
        </div>
      </div>
      <div className={classes.wraperInformation}>
        <div className={classes.listInformation}>
          {listInformation.map((item) => {
            return (
              <LazyLoad>
                <InformationItem
                  key={item.title}
                  icon={item.icon}
                  title={item.title}
                  content={item.content}
                />
              </LazyLoad>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Information;

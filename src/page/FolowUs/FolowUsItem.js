import React from "react";
import HttpsOutlinedIcon from "@mui/icons-material/HttpsOutlined";
import {useStyles} from "./styles"

const FolowUsItem = ({img}) => {
    const classes=useStyles()
    return (
      <div className={classes.itemCard}>
        <img className={classes.img} src={img} alt="fsdfs" />
        <span
          style={{
            position: "absolute",
            top: 18,
            right: 18,
            width: 24,
            height: 24,
            borderRadius: "50%",
            backgroundColor: "#111111",
          }}
        >
          <HttpsOutlinedIcon
            style={{ fontSize: 14, padding: 5, color: "#FFF" }}
          />
        </span>
      </div>
    );
}

export default FolowUsItem

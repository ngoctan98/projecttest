import React, {useState} from "react";
import HeaderContent from "../../component/HeaderContent";
import { listFolowUs } from "../../contants/contants";
import FolowUsOtem from "./FolowUsItem"
import { useStyles } from "./styles";
import LazyLoad from "react-lazyload";

const TopCategori = () => {
    const [width, setWidth] = useState(1910);
    window.addEventListener(
      "resize",
      function (event) {
        setWidth(event.currentTarget.innerWidth);
      },
      true
    );
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <HeaderContent
        header={"Follow Us"}
        content={
          "Inspire and let yourself be inspired, from one unique fashion to another."
        }
      />
      <div className={classes.listItem}>
        {listFolowUs.map((item, index) => {
          return (
            <LazyLoad>
              {width < 1024 && width > 740 && index < 3 && (
                <FolowUsOtem img={item.img} />
              )}
              {width > 1024 &&  (
                <FolowUsOtem img={item.img} />
              )}
              {width < 740 && index < 1 && (
                <FolowUsOtem img={item.img} />
              )}
            </LazyLoad>
          );
        })}
      </div>
    </div>
  );
};
export default TopCategori;

import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((them) => ({
  root: {
    margin: "100px auto 0 auto",
    display: "flex",
    flexDirection: "column",
    maxWidth: 1410,
    borderTop: "solid 1px #ccc",
    paddingTop: 100,
    "@media (max-width: 1024px)": {
      margin: "50px auto 0 auto",
      width: "100%",
      paddingTop: 50,
    },
  },
  listItem: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    marginTop: 100,
    "@media (max-width: 1024px)": {
      marginTop: 50,
    },
  },
  itemCard: {
    position: "relative",
    padding: 8,
  },
  img: {
    backgroundSize: "cover",
    borderRadius: 20,
    width: 218,
    height: 218,
    "@media (max-width: 1024px)": {
      backgroundSize: "cover",
      borderRadius: 20,
      width: 218,
      height: 218,
    },
  },
}));
import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((them) => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    paddingLeft: 30,
    paddingRight: 30,
    height: 60,
    alignItems: "center",
    borderBottom: "solid 1px #ccc",
    "@media (max-width: 740px)": {
      padding: 0,
    },
  },
  headerRight: {
    display: "flex",
    marginRight: 25,
    "@media (max-width: 740px)": {
      marginRight: 8,
    },
  },
  headerLeft: {
    display: "flex",
    marginLeft: 25,
    "@media (max-width: 1024px)": {
      display: "none",
    },
    "@media (max-width: 740px)": {
      marginLeft: 8,
    },
  },
  headerTextRight: {
    marginRight: 25,
    fontSize: 13,
    lineHeight: 18,
  },
  text: {
    fontSize: 13,
    fontWeigh: 600,
    marginLeft: 25,
    lineHeight: 18,
  },
  logo: {
    width: 17,
    height: 20,
    "@media (max-width: 740px)": {
      width: 12,
      height: 14,
    },
  },
  action: {
    display: "flex",
    alignItems: "center",
  },
  marginLeft: {
    marginLeft: 23,
    "& .MuiBadge-badge": {
      background: "#9C6D3A",
    },
    "@media (max-width: 740px)": {
      marginLeft: 8,
    },
  },
  menu: {
    "@media (min-width: 1029px)": {
      display: "none",
    },
  },
}));

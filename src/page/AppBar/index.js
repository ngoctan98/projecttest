/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import DropDown from "../../component/DropDown";
import { useStyles } from "./styles";
import U from "../../assets/u.png";
import M from "../../assets/letter-m.png";
import I from "../../assets/i.png";
import N from "../../assets/letter-n.png";
import O from "../../assets/o.png"
import SearchIcon from "@mui/icons-material/Search";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import Badge from "@mui/material/Badge";
import Menu from "./Menu"

const AppBar = () => {
  const classes = useStyles();
  return (
    <div className={classes.header}>
      <div className={classes.headerRight}>
        <img className={classes.logo} src={U} alt="U"></img>
        <img className={classes.logo} src={M} alt="M"></img>
        <img className={classes.logo} src={I} alt="I"></img>
        <img className={classes.logo} src={N} alt="N"></img>
        <img className={classes.logo} src={O} alt="O"></img>
      </div>
      <div className={classes.headerLeft}>
        <DropDown
          title={"HOME"}
          subList={["vietnamese", "indonesia", "malaysia", "campuchia"]}
          fontWeight={600}
        />
        <DropDown
          title={"SHOP"}
          subList={["VND", "yen", "euro"]}
          fontWeight={600}
        />
        <DropDown
          title={"PRODUCT"}
          subList={["VND", "yen", "euro"]}
          fontWeight={600}
        />
        <DropDown
          title={"PAGE"}
          subList={["VND", "yen", "euro"]}
          fontWeight={600}
        />
        <DropDown
          title={"BLOG"}
          subList={["VND", "yen", "euro"]}
          fontWeight={600}
        />
        <span
          style={{
            fontSize: 13,
            fontWeigh: 600,
            marginLeft: 25,
            lineHeight: "18px",
          }}
        >
          {" "}
          BUY UMINO
        </span>
      </div>
      <div className={classes.action}>
        <SearchIcon size='small' className={classes.marginLeft} />
        <PersonOutlineIcon className={classes.marginLeft} />
        <QueryBuilderIcon className={classes.marginLeft} />
        <Badge badgeContent={4} className={classes.marginLeft}>
          <FavoriteBorderIcon />
        </Badge>
        <Badge badgeContent={4} className={classes.marginLeft}>
          <AddShoppingCartIcon />
        </Badge>
        <Menu
          childrens={[
            <DropDown
              title={"HOME"}
              subList={["vietnamese", "indonesia", "malaysia", "campuchia"]}
              fontWeight={600}
              marginLeft={true}
            />,
            <DropDown
              title={"SHOP"}
              subList={["VND", "yen", "euro"]}
              fontWeight={600}
              marginLeft={true}
            />,
            <DropDown
              title={"PRODUCT"}
              subList={["VND", "yen", "euro"]}
              fontWeight={600}
              marginLeft={true}
            />,
            <DropDown
              title={"PAGE"}
              subList={["VND", "yen", "euro"]}
              fontWeight={600}
              marginLeft={true}
            />,
            <DropDown
              title={"BLOG"}
              subList={["VND", "yen", "euro"]}
              fontWeight={600}
              marginLeft={true}
            />,
            <span
              style={{
                fontSize: 13,
                fontWeigh: 600,
                marginLeft: 25,
                lineHeight: "18px",
              }}
            >
              {" "}
              BUY UMINO
            </span>,
          ]}
        />
      </div>
    </div>
  );
};
export default AppBar;

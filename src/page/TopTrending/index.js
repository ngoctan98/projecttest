import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import HeaderContent from "../../component/HeaderContent";
import CardItem from "./CardItem";
import { listTopTrending } from "../../contants/contants";
import { useStyles } from "./styles";
import LazyLoad from "react-lazyload";


const TopTrending = () => {
    const [width, setWidth] =useState(1910)
    window.addEventListener('resize', function(event) {
        setWidth(event.currentTarget.innerWidth);
    }, true);
    
    const classes=useStyles()
  return (
    <div className={classes.wraperHeader}>
      <HeaderContent
        header={"Top Trending"}
        content={
          "Our products are designed for everyone, environmentally friendly."
        }
      />
      <div className={classes.wraperCard}>
        <Grid container xs={12} spacing={2}>
          {listTopTrending.map((item, index) => {
            return (
              <LazyLoad>
                <Grid
                  key={index}
                  item
                  xs={width < 1024 ? (width < 740 ? 12 : 4) : 3}
                >
                  {width < 741 && index < 2 && (
                    <CardItem
                      img={item.img}
                      content={item.content}
                      discount={item?.discount}
                      preOrder={item?.preOrder}
                      costOld={item?.costOld}
                      cost={item?.cost}
                    />
                  )}
                  {width > 741 && width < 1025 && index < 6 && (
                    <CardItem
                      img={item.img}
                      content={item.content}
                      discount={item?.discount}
                      preOrder={item?.preOrder}
                      costOld={item?.costOld}
                      cost={item?.cost}
                    />
                  )}
                  {width > 1024 && (
                    <CardItem
                      img={item.img}
                      content={item.content}
                      discount={item?.discount}
                      preOrder={item?.preOrder}
                      costOld={item?.costOld}
                      cost={item?.cost}
                    />
                  )}
                </Grid>
              </LazyLoad>
            );
          })}
        </Grid>
      </div>
      <button className={classes.buttonLoadMore}>LOAD MORE</button>
    </div>
  );
};
export default TopTrending;

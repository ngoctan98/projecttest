import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((them) => ({
  root: {
    position: "relative",
    "@media (max-width: 740px)": {
      maxWidth: 340,
      margin: "auto",
    },
  },
  wraperHeader: {
    margin: "100px auto 0 auto",
    display: "flex",
    flexDirection: "column",
    maxWidth: 1410,
    "@media (max-width: 1024px)": {
      margin: "50px auto 0 auto",
    },
  },
  wraperCard: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 40,
    width: "100%",
    "@media (max-width: 1024px)": {
      "& .MuiGrid-root MuiGrid-item": {
        width: "33.33%",
      },
      marginLeft: 8,
    },
  },
  buttonLoadMore: {
    fontSize: 12,
    fontWeight: 600,
    backgroundColor: "#FFFF",
    border: "1px solid #DEDEDE",
    borderRadius: 300,
    height: 50,
    width: 200,
    margin: "40px auto 0",
    "@media (max-width: 1024px)": {
      height: 40,
      width: 150,
      margin: "20px auto 0",
      fontSize: 10,
    },
  },
  cardImg: {
    backgroundSize: "cover",
    width: "100%",
    maxWidth: 340,
    height: 420,
    borderRadius: 10,
    "@media (max-width: 1024px)": {
      height: 370,
    },
  },
  discountWraper: {
    position: "absolute",
    top: 15,
    left: 15,
  },
  discountChip: {
    color: "white",
    fontSize: 12,
    fontWeight: 500,
    lineHeight: "16px",
    padding: "5px 10px",
    borderRadius: 20,
    backgroundColor: "#9C6D3A",
    margin: 0,
  },
  preOrderChip: {
    color: "white",
    fontSize: 12,
    fontWeight: 500,
    lineHeight: "16px",
    padding: "5px 10px",
    borderRadius: 20,
    backgroundColor: "#177983",
    marginTop: 3,
  },
  textContent: {
    fontSize: 14,
    lineHeight: "20px",
    margin: "16px 0 0 0",
    textAlign: "left",
  },
  rating: {
    width: "100%",
    fontSize: 10,
    lineHeight: "10px",
    marginTop: 6,
  },
  costWraper: {
    width: "100%",
    display: "flex",
    alignItems: "flex-end",
    marginTop: 10,
  },
  costSale: {
    textAlign: "left",
    fontSize: 18,
    lineHeight: "22px",
    color: "#9C6D3A",
    margin: 0,
  },
  costOld: {
    textAlign: "left",
    fontSize: 14,
    lineHeight: "20px",
    color: "#8D979E",
    margin: "0 0 0 4px",
    textDecorationLine: "line-through",
  },
  cost: {
    textAlign: "left",
    fontSize: 18,
    lineHeight: "22px",
    color: "#11111",
    margin: 0,
  },
}));

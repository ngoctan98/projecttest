import React from "react";
import {useStyles} from "./styles"
import Rating from "@mui/material/Rating";

const CardItem = ({ content, img, discount, preOrder, costOld, cost}) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <img className={classes.cardImg} alt={"sjlkfdo"} src={img} />
      {discount && <div className={classes.discountWraper}>
        <p className={classes.discountChip}>{discount}</p>
        <p className={classes.preOrderChip}>{preOrder}</p>
      </div>}
      <p className={classes.textContent}>{content}</p>
      <Rating className={classes.rating} value={4} readOnly />
      <span className={classes.costWraper}>
        <p className={classes[discount ? 'costSale' : 'cost']}>{cost}</p>
       {discount && <p className={classes.costOld}>{costOld}</p>}
      </span>
    </div>
  );
};
export default CardItem;

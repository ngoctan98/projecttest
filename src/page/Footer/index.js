import React from "react";
import DropDown from "../../component/DropDown";
import U from "../../assets/u.png";
import M from "../../assets/letter-m.png";
import I from "../../assets/i.png";
import N from "../../assets/letter-n.png";
import O from "../../assets/o.png";
import {useStyles} from "./styles"

const listSub1 = [
  "268 St, South New York/NY 98944, United States",
  "+222-1800-2628",
  "blueskytechcompany@gmail.com",
];
const listSub2 = [
  "Privacy Policy",
  "Refund Policy",
  "Shipping & Return",
  "Term & Conditions",
  "Advanced Search",
  "Store Locations",
];
const listSub3=["Smartphone", "Headphones", "Computer & Desktop", "Cameras & Photos", "Laptop & Ipad"]

const Footer = () => {
    const classes=useStyles()
  return (
    <div className={classes.root}>
      <div className={classes.wraper}>
        <div className={classes.display}>
          <div className={classes.height}>
            <div style={{ textAlign: "left" }}>
              <img className={classes.logo} src={U} alt="U"></img>
              <img className={classes.logo} src={M} alt="M"></img>
              <img className={classes.logo} src={I} alt="I"></img>
              <img className={classes.logo} src={N} alt="N"></img>
              <img className={classes.logo} src={O} alt="O"></img>
            </div>
            <div className={classes.marginTop}>
              {listSub1.map((item) => {
                return <p className={classes.textContent}>{item}</p>;
              })}
            </div>
          </div>
          <div className={classes.height}>
            <p className={classes.textHeader}>CUSTOMER SERVICE</p>
            <div className={classes.marginTop}>
              {listSub2.map((item) => {
                return <p className={classes.textContent}>{item}</p>;
              })}
            </div>
          </div>
        </div>
        <div className={classes.display}>
          <div className={classes.height}>
            <p className={classes.textHeader}>SHOP BY CATEGORIES</p>
            <div className={classes.marginTop}>
              {listSub3.map((item) => {
                return <p className={classes.textContent}>{item}</p>;
              })}
            </div>
          </div>
          <div className={classes.height}>
            <p className={classes.textHeader}>SIGN UP TO NEWSLETTER</p>
            <div>
              <p className={classes.textContentSignUp}>
                Enter your email address to get $10 off your first order and
                free shipping. Updates information on Sales and Offers.
              </p>
            </div>
            <div className={classes.wraperButton}>
              <input
                className={classes.input}
                placeholder="Enter your email..."
                type="email"
              ></input>
              <button className={classes.button}> SUBSCRIBE</button>
            </div>
          </div>
        </div>
      </div>
      <div className={classes.header}>
        <div className={classes.headerRight}>
          <DropDown
            title={"English"}
            subList={["vietnamese", "indonesia", "malaysia", "campuchia"]}
          />
          <DropDown
            title={"United States (USD $)"}
            subList={["VND", "yen", "euro"]}
          />
        </div>
        <div>
          <span className={classes.text}>
            © 2022 Umino Store. All Rights Reserved.
          </span>
        </div>
        <div className={classes.headerLeft}>
          <span className={classes.headerTextLeft}>+222-1800-2628</span>
          <span className={classes.headerTextLeft}>
            blueskytechcompany@gmail.com
          </span>
        </div>
      </div>
    </div>
  );
};

export default Footer;

import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((them) => ({
  root: {
    margin: "100px auto 0 auto",
    display: "flex",
    flexDirection: "column",
    borderTop: "solid 1px #ccc",
    paddingTop: 100,
    backgroundColor: "#F5F5F5",
    "@media (max-width: 1024px)": {
      margin: "50px auto 0 auto",
      paddingTop: 50,
    },
  },
  wraper: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
    maxWidth: 1410,
    margin: "auto",
    "@media (max-width: 740px)": {
     flexDirection: 'column'
    },
  },
  headerRight: {
    display: "flex",
    marginRight: -25,
  },
  headerLeft: {
    display: "flex",
    marginLeft: 25,
  },
  headerTextRight: {
    fontSize: 13,
    lineHeight: 18,
  },
  headerTextLeft: {
    marginLeft: 25,
    fontSize: 13,
    lineHeight: 18,
  },
  textContent: {
    textAlign: "left",
    fontSize: 14,
    lineHeight: "19px",
  },
  textContentSignUp: {
    textAlign: "left",
    fontSize: 14,
    lineHeight: "19px",
    maxWidth: 450,
  },
  logo: {
    width: 17,
    height: 20,
  },
  marginTop: {
    marginTop: 25,
  },
  textHeader: {
    textAlign: "left",
    fontSize: 13,
    fontWeight: 600,
    lineHeight: "18px",
    margin: 0,
  },
  input: {
    width: 290,
    height: 50,
    borderRadius: 30,
    border: "none",
    paddingLeft: 16,
  },
  button: {
    width: 150,
    height: 50,
    borderRadius: 30,
    border: "none",
    backgroundColor: "#111111",
    marginLeft: 10,
    color: "#FFF",
    "@media (max-width: 1024px)": {
      marginTop: 20,
    },
  },
  text: {
    fontSize: 13,
    lineHeight: 18,
  },
  header: {
    width: "100%",
    maxWidth: 1410,
    display: "flex",
    justifyContent: "space-between",
    height: 96,
    alignItems: "center",
    borderTop: "solid 1px #ccc",
    margin: "auto",
    "@media (max-width: 1024px)": {
      display: "none",
    },
  },
  display: {
    display: "flex",
    "@media (max-width: 1024px)": {
      display: "initial",
      padding: 16,
    },
  },
  height: {
    "@media (max-width: 1024px)": {
      height: 300,
    },
  },
  wraperButton: {
    display: "flex",
    "@media (max-width: 1024px)": {
      flexDirection: "column",
    },
  },
}));

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import DropDown from "../../component/DropDown";
import { useStyles } from "./styles";

const Header = () => {
    const classes=useStyles()
    return (
      <div className={classes.header}>
        <div className={classes.headerRight}>
          <span className={classes.headerTextRight}>+222-1800-2628</span>
          <span className={classes.headerTextRight}>
            blueskytechcompany@gmail.com
          </span>
        </div>
        <div>
          <span className={classes.text}>
            Sign up for 10% off your first order:{" "}
          </span>
          <a className={classes.text}>Sign Up</a>
        </div>
        <div className={classes.headerLeft}>
          <span className={classes.headerTextLeft}>Our Stores</span>
          <DropDown
            marginLeft={true}
            title={"English"}
            subList={["vietnamese", "indonesia", "malaysia", "campuchia"]}
          />
          <DropDown
            marginLeft={true}
            title={"United States (USD $)"}
            subList={["VND", "yen", "euro"]}
          />
        </div>
      </div>
    );
}
export default Header
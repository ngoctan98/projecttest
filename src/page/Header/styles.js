import {makeStyles} from "@mui/styles"

export const useStyles = makeStyles((them) => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    paddingLeft: 30,
    paddingRight: 30,
    height: 40,
    alignItems: "center",
    borderBottom: "solid 1px #ccc",
    "@media (max-width: 740px)": {
      display: "none",
    },
  },
  headerRight: {
    display: "flex",
    marginRight: 25,
    "@media (max-width: 1024px)": {
      display: "none",
    },
  },
  headerLeft: {
    display: "flex",
    marginLeft: 25,
  },
  headerTextRight: {
    marginRight: 25,
    fontSize: 13,
    lineHeight: 18,
  },
  headerTextLeft: {
    marginLeft: 25,
    fontSize: 13,
    lineHeight: 18,
  },
  text: {
    fontSize: 13,
    lineHeight: 18,
  },
}));
